const express = require('express')
const wordRouter = express.Router()
const wordController = require('./words.controller')

wordRouter.get('/', wordController.getAllWords)
wordRouter.get('/random-word/:difficulty', wordController.getRandomWord)
wordRouter.post('/', wordController.postNewWord)
wordRouter.delete('/:id', wordController.deleteWord)

module.exports = wordRouter
