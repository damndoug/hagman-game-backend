const mongoose = require('mongoose')
const Schema = mongoose.Schema

const WordsSchema = new Schema({
  word: {
    type: String,
    required: 'No new word was provided'
  },
  tip: {
    type: String,
    default: ''
  },
  difficulty: {
    type: Object,
    required: 'No difficulty was provided'
  },
  date: {
    type: String
  }
})

module.exports = mongoose.model('Words', WordsSchema)
