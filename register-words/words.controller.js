require('./words.schema')

const mongoose = require('mongoose')
const Words = mongoose.model('Words')

exports.getAllWords = (req, res) => {
  Words.find((err, words) => {
    if (err) res.send(err)
    res.json(words)
  })
}

exports.postNewWord = (req, res) => {
  const newWord = new Words(req.body)
  newWord.save((err, word) => {
    if (err) return res.send(err)
    res.json(word)
  })
}

exports.deleteWord = (req, res) => {
  Words.findByIdAndRemove({ _id: req.params.id }, (err, status) => {
    if (err) return res.send(err)
    res.json(status)
  })
}

exports.getRandomWord = (req, res) => {
  Words.aggregate()
    .sample(1)
    .match({ difficulty: req.params.difficulty }).then((result) => {
    res.json(result)
  })
}
