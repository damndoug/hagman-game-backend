const express = require('express')
const app = express()
const port = 3000
const cors = require('cors')
const bodyParser = require('body-parser')
const wordsRouter = require('./register-words')
const rankingRouter = require('./ranking')
const mongoose = require('mongoose')

mongoose.connect('mongodb://127.0.0.1/forca-db')
//mongoose.connect('root:parangoaricutirimirruaro@153.92.209.59:27017')

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(cors())

app.all('/*', function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "X-Requested-With")
  next()
})

app.use('/words', wordsRouter)
app.use('/ranking', rankingRouter)

app.listen(port, () => {
  console.log('Example log listen at ' + port)
})
