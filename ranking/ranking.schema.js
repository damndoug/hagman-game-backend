const mongoose = require('mongoose')
const Schema = mongoose.Schema

const RankingSchema = new Schema({
  username: {
    type: String,
    required: 'A user name was not provided'
  },
  score: {
    type: Number,
    required: 'A user score was not provided'
  },
  totalTime: {
    type: Number,
    required: 'A user score was not provided'
  },
  date: {
    type: Date,
    default: Date.now()
  }
})

module.exports = mongoose.model('Ranking', RankingSchema)
