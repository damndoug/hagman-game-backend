require('./ranking.schema')

const mongoose = require('mongoose')
const Ranking = mongoose.model('Ranking')

exports.getRanking = (req, res) => {
  Ranking.find((err, ranking) => {
    if (err) return res.send(err)
    res.json(ranking)
  })
}

exports.postRanking = (req, res) => {
  const newRanking = new Ranking(req.body)
  newRanking.save((err, ranking) => {
    if (err) return res.send(err)
    res.json(ranking)
  })
}
