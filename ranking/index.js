const express = require('express')
const rankingRouter = express.Router()
const rankingController = require('./ranking.controller')

rankingRouter.get('/', rankingController.getRanking)
rankingRouter.post('/', rankingController.postRanking)

module.exports = rankingRouter
